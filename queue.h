#ifndef QUEUE_H_
# define QUEUE_H_

#include <stdlib.h>

# define QUEUE(Type, Name)                                       \
    LIST(Type, queue_ ## Name);                                  \
    typedef list_queue_ ## Name queue_ ## Name                   \


/* Initialize the data structure */
# define QUEUE_INIT(Name, Queue)                                 \
    LIST_INIT(queue_ ## Name, Queue)                             \


/* Enqueue an element */
# define QUEUE_PUSH(Name, Queue, Value)                          \
    LIST_INSERT_AT(queue_ ## Name, Queue, Value, 0)              \

/* Returns the first element of the queue without removing it */
# define QUEUE_PEEK(Queue)                                       \
    ((Queue)->end->value)                                        \

/* Dequeue the first element */
# define QUEUE_TAKE(Name, Queue, Out)                            \
    do {                                                         \
        Out = QUEUE_PEEK(Queue);                                 \
        LIST_POP(queue_ ## Name, Queue);                         \
    } while (0)                                                  \


/* Returns 1 if the queue is empty */
# define QUEUE_EMPTY(Queue)                                      \
    LIST_EMPTY(Queue)                                            \

/* Returns the number of elements in the queue */
# define QUEUE_SIZE(Queue)                                       \
    LIST_SIZE(Queue)                                             \

/* Destroy the stack and all its content */
# define QUEUE_DESTROY(Name, Queue)                              \
    LIST_DESTROY(queue_ ## Name, Queue)                          \


#endif /* QUEUE_H_ */
