#ifndef STACK_H_
# define STACK_H_

#include <stdlib.h>

# define STACK(Type, Name)                              \
    LIST(Type, stack_ ## Name);                         \
    typedef list_stack_ ## Name stack_ ## Name          \


/* Initialize the data structure */
# define STACK_INIT(Name, Stack)                        \
    LIST_INIT(stack_ ## Name, Stack)                    \


/* Push an element on the top of the stack */
# define STACK_PUSH(Name, Stack, Value)                 \
    LIST_APPEND(stack_ ## Name, Stack, Value)           \

/* Get the top element of the stack */
# define STACK_TOP(Stack)                               \
    ((Stack)->end->value)                               \

/* Remove the top element of the stack and return it */
# define STACK_POP(Name, Stack, Out)                    \
    do {                                                \
        Out = STACK_TOP(Stack);                         \
        LIST_POP(stack_ ## Name, Stack);                \
    } while (0)                                         \

/* Return 1 if the stack is empty */
# define STACK_EMPTY(Stack)                             \
    LIST_EMPTY(Stack)                                   \

/* Return the number of elements in the stack */
# define STACK_SIZE(Stack)                              \
    LIST_SIZE(Stack)                                    \

/* Destroy the stack and all its content */
# define STACK_DESTROY(Name, Stack)                     \
    LIST_DESTROY(stack_ ## Name, Stack)                 \

#endif /* STACK_H_ */
