#ifndef HASHTBL_H_
# define HASHTBL_H_

# define HASHTBL_DEFAULT_SIZE 256

# define HASHTBL(KeyType, ValueType, Name, CompFunc)                          \
    typedef struct hashtbl_item_##Name                                        \
    {                                                                         \
        KeyType key;                                                          \
        ValueType value;                                                      \
    } hashtbl_item_##Name;                                                    \
    LIST(hashtbl_item_##Name, hashtbl_##Name);                                \
    typedef struct hashtbl_body_##Name {                                      \
        list_hashtbl_##Name *table;                                           \
        int size;                                                             \
    } hashtbl_body_##Name;                                                    \
    int hashtbl_compfunc_##Name(KeyType a, hashtbl_item_##Name b) \
    {                                                                         \
        return CompFunc(a, b.key);                                            \
    }                                                                         \
    typedef hashtbl_body_##Name *hashtbl_##Name;                              \
    typedef KeyType hashtbl_keytype_##Name;                                   \
    typedef ValueType hashtbl_valuetype_##Name                                \


/* Hash function (using the djb2 algorithm) with a very good distribution */
# define HASHTBL_HASH(Name, Key, TblSize, KeySize, Out)                       \
    do {                                                                      \
        Out = 133742;                                                         \
        int _hashtbl_i = 0;                                                   \
        while (_hashtbl_i < KeySize)                                          \
            Out = (((Out << 5) + Out + Key[_hashtbl_i++]) % TblSize);         \
    } while (0)                                                               \


/* Initialize the data type */
# define HASHTBL_INIT(Name, Tbl, Size)                                        \
    do {                                                                      \
        Tbl = malloc(sizeof(hashtbl_body_##Name));                            \
        if (!Size)                                                            \
            Tbl->size = HASHTBL_DEFAULT_SIZE;                                 \
        else                                                                  \
            Tbl->size = Size;                                                 \
        Tbl->table = calloc(Tbl->size, sizeof(list_hashtbl_##Name));          \
    } while(0)                                                                \


/* Find an element in the hashtable by Key and retrive the corresponding
 * list_node */
# define HASHTBL_FIND(Name, Tbl, Key, KeySize, OutNode, OutHash)              \
    do {                                                                      \
        int _hashtbl_unused;                                                  \
        HASHTBL_HASH(Name, Key, Tbl->size, KeySize, OutHash);                 \
        LIST_FIND(hashtbl_##Name, Tbl->table[OutHash],                        \
                hashtbl_compfunc_##Name, Key, _hashtbl_unused, OutNode);      \
        (void) _hashtbl_unused;                                               \
    } while (0)                                                               \

/* Get the value of the element designated by Key */
# define HASHTBL_GET(Name, Tbl, Key, KeySize, OutValue)                       \
    do {                                                                      \
        list_node_hashtbl_##Name *_hashtbl_p = NULL;                          \
        int _hashtbl_h;                                                       \
        HASHTBL_FIND(Name, Tbl, Key, KeySize, _hashtbl_p, _hashtbl_h);        \
        if (_hashtbl_p)                                                       \
            OutValue = _hashtbl_p->value.value;                               \
        (void) _hashtbl_h;                                                    \
    } while (0)                                                               \

/* Set an element in the table */
# define HASHTBL_SET(Name, Tbl, Key, KeySize, Value)                          \
    do {                                                                      \
        list_node_hashtbl_##Name *_hashtbl_p = NULL;                           \
        int _hashtbl_h;                                                       \
        HASHTBL_FIND(Name, Tbl, Key, KeySize, _hashtbl_p, _hashtbl_h);        \
        if (_hashtbl_p)                                                       \
            LIST_REMOVE(hashtbl_##Name, Tbl->table[_hashtbl_h], _hashtbl_p);  \
        LIST_APPEND(hashtbl_##Name, Tbl->table[_hashtbl_h],                   \
                ((hashtbl_item_##Name) { Key, Value }));                      \
    } while (0)

/* Remove an element in the table */
# define HASHTBL_REMOVE(Name, Tbl, Key, KeySize)                              \
    do {                                                                      \
        list_node_hashtbl_##Name _hashtbl_p = NULL;                           \
        int _hashtbl_h;                                                       \
        HASHTBL_FIND(Name, Tbl, Key, KeySize, _hashtbl_p, _hashtbl_h);        \
        if (_hashtbl_p)                                                       \
            LIST_REMOVE(hashtbl_##Name, Tbl[_hashtbl_h], _hashtbl_p);         \
    } while (0)                                                               \

/* Destroy the hashtbl and all its content */
# define HASHTBL_DESTROY(Name, Tbl)                                           \
    do {                                                                      \
        for (int i = 0; i < Tbl->size; i++)                                   \
            LIST_DESTROY(hasthbl_##Name, Tbl->table[i];                       \
    while (0);                                                                \

#endif /* HASHTBL_H_ */
