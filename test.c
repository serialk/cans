#include <stdio.h>
#include <string.h>
#include "list.h"
#include "stack.h"
#include "queue.h"
#include "hashtbl.h"

LIST(int, int);
STACK(int, int);
QUEUE(int, int);
HASHTBL(char*, int, charint, !strcmp);

int test1(int a)
{
    return a * 2;
}

void display(int v)
{
    printf("%d, ", v);
}

int main(void)
{
    list_int lol;
    LIST_INIT(int, lol);

    LIST_APPEND(int, lol, 3);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_APPEND(int, lol, 4);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_APPEND(int, lol, 5);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_APPEND(int, lol, 6);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_REMOVE(int, lol, lol->start);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_REMOVE(int, lol, lol->start);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_REMOVE(int, lol, lol->end);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_REMOVE(int, lol, lol->end);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_APPEND(int, lol, 42);
    LIST_APPEND(int, lol, 43);
    LIST_APPEND(int, lol, 44);

    LIST_INSERT_AT(int, lol, 3, 1);
    LIST_INSERT_AT(int, lol, 4, 2);

    LIST_INSERT(int, lol, 38749347, lol->start->next->next);

    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_REMOVE(int, lol, lol->start->next);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_REMOVE(int, lol, lol->start->next);
    LIST_ITER(int, lol, display);
    printf("\n-> ");

    LIST_POP(int, lol);
    LIST_ITER(int, lol, display);
    printf("\n-> ");


    LIST_DESTROY(int, lol);


    stack_int st;
    STACK_INIT(int, st);
    STACK_PUSH(int, st, 3);
    LIST_ITER(stack_int, st, display); printf("\n");
    STACK_PUSH(int, st, 4);
    LIST_ITER(stack_int, st, display); printf("\n");
    STACK_PUSH(int, st, 5);
    LIST_ITER(stack_int, st, display); printf("\n");
    STACK_PUSH(int, st, 6);
    LIST_ITER(stack_int, st, display); printf("\n");
    STACK_PUSH(int, st, 7);
    LIST_ITER(stack_int, st, display); printf("\n");
    int i;
    STACK_POP(int, st, i);
    LIST_ITER(stack_int, st, display); printf("\n");
    STACK_POP(int, st, i);
    LIST_ITER(stack_int, st, display); printf("\n");
    STACK_POP(int, st, i);
    LIST_ITER(stack_int, st, display); printf("\n");
    STACK_POP(int, st, i);
    LIST_ITER(stack_int, st, display); printf("\n");
    STACK_POP(int, st, i);
    LIST_ITER(stack_int, st, display); printf("\n");

    STACK_DESTROY(int, st);

    queue_int q;
    QUEUE_INIT(int, q);
    QUEUE_PUSH(int, q, 3);
    LIST_ITER(queue_int, q, display); printf("\n");
    QUEUE_PUSH(int, q, 4);
    LIST_ITER(queue_int, q, display); printf("\n");
    QUEUE_PUSH(int, q, 5);
    LIST_ITER(queue_int, q, display); printf("\n");
    QUEUE_PUSH(int, q, 6);
    LIST_ITER(queue_int, q, display); printf("\n");
    QUEUE_PUSH(int, q, 7);
    LIST_ITER(queue_int, q, display); printf("\n");
    QUEUE_TAKE(int, q, i);
    LIST_ITER(queue_int, q, display); printf("\n");
    QUEUE_TAKE(int, q, i);
    LIST_ITER(queue_int, q, display); printf("\n");
    QUEUE_TAKE(int, q, i);
    LIST_ITER(queue_int, q, display); printf("\n");
    QUEUE_TAKE(int, q, i);
    LIST_ITER(queue_int, q, display); printf("\n");
    QUEUE_TAKE(int, q, i);
    LIST_ITER(queue_int, q, display); printf("\n");

    QUEUE_DESTROY(int, q);


    hashtbl_charint tbl;
    HASHTBL_INIT(charint, tbl, 0);
    list_node_hashtbl_charint *koin = NULL;
    HASHTBL_SET(charint, tbl, "lol", 4, 3);
    /*HASHTBL_SET(charint, tbl, 3, 4);
    HASHTBL_SET(charint, tbl, 4, 2);
    HASHTBL_SET(charint, tbl, 2, 8);
    HASHTBL_REMOVE(charint, tbl, 2); */

    (void) i;
    (void) koin;

    return EXIT_SUCCESS;
}
