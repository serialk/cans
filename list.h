#ifndef LIST_H_
# define LIST_H_

#include <stdlib.h>

# define LIST(Type, Name)                                                   \
    typedef struct list_node_ ## Name                                       \
    {                                                                       \
        Type value;                                                         \
        struct list_node_ ## Name *next;                                    \
        struct list_node_ ## Name *prev;                                    \
    } list_node_ ## Name;                                                   \
    typedef struct list_body_ ## Name                                       \
    {                                                                       \
        list_node_ ## Name *start;                                          \
        list_node_ ## Name *end;                                            \
        int count;                                                          \
    } list_body_ ## Name;                                                   \
    typedef list_body_ ## Name *list_ ## Name                               \

/* Initialize the list structure */
# define LIST_INIT(Name, List)                                              \
    do {                                                                    \
        List = malloc(sizeof(list_body_ ## Name));                          \
        List->start = NULL;                                                 \
        List->end = NULL;                                                   \
        List->count = 0;                                                    \
    } while(0)                                                              \

/* Get the Nth element of the list */
# define LIST_NTH(Name, List, Index, OutNode)                               \
    do {                                                                    \
        int _list_i;                                                        \
        list_node_ ## Name *_list_p;                                        \
        OutNode = NULL;                                                     \
        if (Index > List->count / 2)                                        \
        {                                                                   \
            _list_i = List->count - 1;                                      \
            _list_p = List->end;                                            \
            while (_list_i > Index && _list_p && _list_p->prev)             \
            {                                                               \
                _list_i--;                                                  \
                _list_p = _list_p->prev;                                    \
            }                                                               \
        }                                                                   \
        else                                                                \
        {                                                                   \
            _list_i = 0;                                                    \
            _list_p = List->start;                                          \
            while (_list_i < Index && _list_p && _list_p->next)             \
            {                                                               \
                _list_i++;                                                  \
                _list_p = _list_p->next;                                    \
            }                                                               \
        }                                                                   \
        if (_list_i == Index)                                               \
            OutNode = _list_p;                                              \
    } while (0)

/* Insert an element in the list after PreviousNode (at the beginning if
 * PreviousNode is NULL). */
# define LIST_INSERT(Name, List, Value, PreviousNode)                       \
    do {                                                                    \
        list_node_ ## Name *_list_p = PreviousNode;                         \
        list_node_ ## Name *_list_n = malloc(                               \
                sizeof(list_node_ ## Name));                                \
        _list_n->value = Value;                                             \
        _list_n->prev = _list_p;                                            \
        if (_list_p)                                                        \
        {                                                                   \
            if (_list_p->next)                                              \
            {                                                               \
                _list_n->next = _list_p->next;                              \
                _list_n->next->prev = _list_n;                              \
            }                                                               \
            else                                                            \
            {                                                               \
                _list_n->next = NULL;                                       \
                List->end = _list_n;                                        \
            }                                                               \
            _list_p->next = _list_n;                                        \
        }                                                                   \
        else                                                                \
        {                                                                   \
            if (List->start)                                                \
            {                                                               \
                _list_n->next = List->start;                                \
                List->start->prev = _list_n;                                \
                List->start = _list_n;                                      \
            }                                                               \
            else                                                            \
            {                                                               \
                List->start = _list_n;                                      \
                List->end = _list_n;                                        \
                _list_n->next = NULL;                                       \
            }                                                               \
        }                                                                   \
        List->count++;                                                      \
    } while (0)

/* Remove an element at the specified position in the list */
# define LIST_REMOVE(Name, List, Node)                                      \
    do {                                                                    \
        list_node_ ## Name *_list_p = Node;                                 \
        if (_list_p)                                                        \
        {                                                                   \
            if (_list_p->next)                                              \
                _list_p->next->prev = _list_p->prev;                        \
            else                                                            \
                List->end = _list_p->prev;                                  \
            if (_list_p->prev)                                              \
                _list_p->prev->next = _list_p->next;                        \
            else                                                            \
                List->start = _list_p->next;                                \
            List->count--;                                                  \
            free(_list_p);                                                  \
        }                                                                   \
    } while (0)

/* Returns 1 if the list is empty */
# define LIST_EMPTY(List) (!List->count)

/* Returns the number of elements in the list */
# define LIST_SIZE(List) (List->count)

/* Returns the number of elements in the list */
# define LIST_VALUE(Node) (Node->value)


/* Append an element at the end of the list */
# define LIST_APPEND(Name, List, Value)                                     \
        LIST_INSERT(Name, List, Value, List->end)

/* Remove an element at the end of the list */
# define LIST_POP(Name, List)                                               \
        LIST_REMOVE(Name, List, List->end)


/* Add an element at the given position */
# define LIST_INSERT_AT(Name, List, Value, Index)                           \
    do {                                                                    \
        list_node_ ## Name *_list_a = List->end;                            \
        if (Index == 0)                                                     \
            _list_a = NULL;                                                 \
        else                                                                \
            LIST_NTH(Name, List, Index - 1, _list_a);                       \
        LIST_INSERT(Name, List, Value, _list_a);                            \
    } while (0)                                                             \

/* Remove the element at the given position */
# define LIST_REMOVE_AT(Name, List, Index)                                  \
    do {                                                                    \
        list_node_ ## Name *_list_a = NULL;                                 \
        LIST_NTH(Name, List, Index, _list_a);                               \
        LIST_REMOVE(Name, List, _list_a);                                   \
    } while (0)                                                             \


/* Execute some code for each element.
 * _list_pos contains your current position in the list.
 * _list_elt contains the current element. */
# define LIST_FOREACH(Name, List, Body)                                     \
    do {                                                                    \
        list_node_ ## Name *_list_elt = List->start;                        \
        int _list_pos = 0;                                                  \
        while (_list_elt)                                                   \
        {                                                                   \
            Body                                                            \
            _list_elt = _list_elt->next;                                    \
            _list_pos++;                                                    \
        }                                                                   \
    } while(0)                                                              \

/* Find the first element where CompFunc(element, Value) is true and
 * retrieve its index and the corresponding list_node */
# define LIST_FIND(Name, List, CompFunc, Value, OutIndex, OutNode)          \
    do {                                                                    \
        list_node_ ## Name *_list_elt = List->start;                        \
        int _list_pos = 0;                                                  \
        OutIndex = -1;                                                      \
        OutNode = NULL;                                                     \
        while (_list_elt)                                                   \
        {                                                                   \
            if (CompFunc(Value, _list_elt->value))                          \
            {                                                               \
                OutIndex = _list_pos;                                       \
                OutNode = _list_elt;                                        \
                break;                                                      \
            }                                                               \
            _list_elt = _list_elt->next;                                    \
            _list_pos++;                                                    \
        }                                                                   \
    } while(0)                                                              \


/* Apply Function to every item of the list and replace it by its result */
# define LIST_MAP(Name, List, Func)                                         \
    LIST_FOREACH(Name, List, _list_elt->value = Func(_list_elt->value);)    \

/* Apply Function to every item of the list */
# define LIST_ITER(Name, List, Func)                                        \
    LIST_FOREACH(Name, List, Func(_list_elt->value);)                       \

/* Apply Function of two arguments cumulatively to the items of List,
 * to reduce the iterable to a single value */
# define LIST_REDUCE(Name, List, Func, OutValue)                            \
    do {                                                                    \
        list_node_ ## Name *_list_elt = List->start;                        \
        if (_list_elt)                                                      \
        {                                                                   \
            OutValue = _list_elt->value;                                    \
            while (_list_elt && _list_elt->next)                            \
            {                                                               \
                _list_elt = _list_elt->next;                                \
                OutValue = Func(OutValue, _list_elt->value);                \
            }                                                               \
        }                                                                   \
    } while(0)                                                              \

/* Destroy the list and all its content */
# define LIST_DESTROY(Name, List)                                           \
    do {                                                                    \
        list_node_ ## Name *_list_old;                                      \
        while (List->start)                                                 \
        {                                                                   \
            _list_old = List->start;                                        \
            List->start = List->start->next;                                \
            free(_list_old);                                                \
        }                                                                   \
        free(List);                                                         \
    } while (0)                                                             \

#endif /* LIST_H_ */
